<?php

namespace App\Tests\acceptance;

use App\Tests\AcceptanceTester;

/**
 * Class DefaultControllerCest
 * @package App\Tests\acceptance
 */
class DefaultControllerCest
{
    /**
     * @param AcceptanceTester $I
     */
    public function _before(AcceptanceTester $I)
    {

    }

    /**
     * Test that landing page shows expected content
     * @param AcceptanceTester $I
     */
    public function landingPageTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('Advent of Code Challenge');
        $I->see('Day 8');
        $I->see('Please click on the links below to navigate');
    }

    /**
     * Test that navigation to part one is successful
     * @param AcceptanceTester $I
     */
    public function navigateToPartOneTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->click('Part One');
        $I->see('Advent of Code Challenge - Day 8 part one');
    }

    /**
     * Test that navigation to part two is successful
     * @param AcceptanceTester $I
     */
    public function navigateToPartTwoTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->click('Part Two');
        $I->see('Advent of Code Challenge - Day 8 part two');
    }

    /**
     * Test that part one page shows correct information
     * @param AcceptanceTester $I
     */
    public function partOnePageTest(AcceptanceTester $I)
    {
        $I->amOnPage('/one');
        $I->see('Advent of Code Challenge - Day 8 part one');
        $I->see('The answer for part one is 1553');
    }


    /**
     * Test that part two page shows correct information
     * @param AcceptanceTester $I
     */
    public function partTwoPageTest(AcceptanceTester $I)
    {
        $I->amOnPage('/two');
        $I->see('Advent of Code Challenge - Day 8 part two');
        $I->see('The answer for part two is 1877');
    }
}

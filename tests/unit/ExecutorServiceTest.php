<?php

namespace App\Tests\unit;

use App\Service\ExecutorService;
use App\Tests\UnitTester;
use Codeception\Test\Unit;

class ExecutorServiceTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected UnitTester $tester;

    /**
     * @var ExecutorService
     */
    protected ExecutorService $executorService;

    /**
     * Initialize executor service before each test run
     */
    protected function _before()
    {
        $this->executorService->initialize('day8.txt');
    }

    protected function _inject(ExecutorService $executorService)
    {
        $this->executorService = $executorService;
    }


    /**
     * Testing correct initialization of Executor Service
     */
    public function testExecutorServiceIsInitializedCorrectly()
    {
        $this->executorService->initialize('day8.txt');
        $instructionSet = $this->executorService->getInstructionSet();

        $this->assertTrue(count($instructionSet) > 0);
    }


    /**
     * Testing executor service part one returns correct result
     * @depends testExecutorServiceIsInitializedCorrectly
     */
    public function testExecutorServicePartOneReturnsCorrectResult()
    {
        $result = $this->executorService->taskPartOne();

        $this->assertTrue($result == 1553);
    }

    /**
     * Testing executor service part two returns correct result
     * @depends testExecutorServiceIsInitializedCorrectly
     */
    public function testExecutorServicePartTwoReturnsCorrectResult()
    {
        $result = $this->executorService->taskPartTwo();

        $this->assertTrue($result == 1877);
    }
}
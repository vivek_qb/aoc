<?php

namespace App\Tests\unit;

use App\Service\ExecutorService;
use App\Service\FileParserService;
use App\Tests\UnitTester;
use Codeception\Test\Unit;

class ExecutorServiceDifferentInputTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected UnitTester $tester;

    /**
     * @var FileParserService
     */
    protected FileParserService $fileParserService;

    /**
     * @var ExecutorService
     */
    protected ExecutorService $executorService;

    /**
     * Initialize executor service before each test run
     */
    protected function _before()
    {
        $this->executorService->initialize('day8v2.txt');
    }

    protected function _inject(
        FileParserService $fileParserService,
        ExecutorService $executorService
    )
    {
        $this->fileParserService = $fileParserService;
        $this->executorService = $executorService;
    }


    /**
     * Testing correct initialization of Executor Service
     */
    public function testExecutorServiceIsInitializedCorrectly()
    {
        $this->executorService->initialize('day8.txt');
        $instructionSet = $this->executorService->getInstructionSet();

        $this->assertTrue(count($instructionSet) > 0);
    }


    /**
     * Testing executor service part one doesn't return correct value with different input
     * @depends testExecutorServiceIsInitializedCorrectly
     */
    public function testExecutorServicePartOneDoesNotReturnsCorrectResult()
    {
        $result = $this->executorService->taskPartOne();

        $this->assertTrue($result != 1553);
    }

    /**
     * Testing executor service part two doesn't return correct value with different input
     * @depends testExecutorServiceIsInitializedCorrectly
     */
    public function testExecutorServicePartTwoDoesNotReturnsCorrectResult()
    {
        $result = $this->executorService->taskPartTwo();

        $this->assertTrue($result != 1877);
    }
}
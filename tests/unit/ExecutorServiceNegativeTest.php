<?php

namespace App\Tests\unit;

use App\Service\ExecutorService;
use App\Tests\UnitTester;
use Codeception\Test\Unit;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class ExecutorServiceNegativeTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected UnitTester $tester;

    /**
     * @var ExecutorService
     */
    protected ExecutorService $executorService;

    /**
     * @param ExecutorService $executorService
     */
    protected function _inject(ExecutorService $executorService)
    {
        $this->executorService = $executorService;
    }

    /**
     * Testing executor service is not initialized with wrong file name.
     */
    public function testExecutorServiceIsNotInitializedWithWrongFileName()
    {
        $this->tester->expectThrowable(FileNotFoundException::class, function(){
            $this->executorService->initialize('WrongFileName.txt');
        });
    }

}
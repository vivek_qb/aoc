Symfony Application | Advent Of Code | Day 8
==================================


## PreRequisites

* PHP 7.4
* Composer


## Installation


### Application Setup

**open control panel and move to the source root directory**:

**Install dependencies with composer**:
```bash
$ composer install
```
### Server Setup
Setup a compatible web server (i.e. Apache, Nginx) with root directory targeting to Public directory of the application.

or

Start a local web server
```bash
$ symfony server:start
```
## Source Code

The developer generated code resides in the following php files: 

* **Controller**
	* src/Controller/DefaultController
* **Service**
	* src/Services/ExecutorService
    * src/Services/FileParserService
* **Unit Test**
	* tests/unit/ExecutorServiceTest
    * tests/unit/ExecutorServiceNegativeTest
    * tests/unit/ExecutorServiceDifferentInputTest
* **Acceptance Test**
	* tests/acceptance/DefaultControllerCest

##  Unit Testing Using Codeception

```bash
$ php vendor/bin/codecept run unit
```

## Acceptance Testing Using Codeception
---------------------------

Change the url in ./acceptance.suite.yml file with your local server or web server url

```bash
modules:
    enabled:
        - PhpBrowser:
            url: http://localhost:8000
        
```

```bash
$ php vendor/bin/codecept run acceptance --steps
```
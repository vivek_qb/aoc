<?php

namespace App\Controller;

use App\Service\ExecutorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @package App\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @var ExecutorService
     */
    private ExecutorService $executorService;

    /**
     * DefaultController constructor.
     * @param ExecutorService $executorService
     */
    public function __construct(ExecutorService $executorService)
    {
        $this->executorService = $executorService;
        $this->executorService->initialize('day8.txt');
    }

    /**
     * @Route("/", name="default")
     * @return Response
     */
    public function indexAction(): Response
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/one", name="one")
     * @return Response
     */
    public function partOneAction(): Response
    {
        $answer = $this->executorService->taskPartOne();
        return $this->render('default/solution.html.twig', [
            'part' => 'one',
            'answer' => $answer
        ]);
    }

    /**
     * @Route("/two", name="two")
     * @return Response
     */
    public function partTwoAction(): Response
    {
        $answer = $this->executorService->taskPartTwo();
        return $this->render('default/solution.html.twig', [
            'part' => 'two',
            'answer' => $answer
        ]);
    }
}

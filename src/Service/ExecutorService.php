<?php

namespace App\Service;

use ArrayObject;

/**
 * Class ExecutorService
 * @package App\Service
 */
class ExecutorService
{
    /**
     * @var array
     */
    private array $executedIndexes = [];

    /**
     * @var array
     */
    private array $instructionSet = [];

    /**
     * @var array
     */
    private array $instructionCopy;

    /**
     * @var bool
     */
    private bool $isPartTwo = false;

    /**
     * @var int
     */
    private int $accumulator = 0;

    /**
     * @var int
     */
    private int $answer;

    /**
     * @var FileParserService
     */
    private FileParserService $fileParserService;

    /**
     * ExecutorService constructor.
     */
    public function __construct(FileParserService $fileParserService)
    {
        $this->fileParserService = $fileParserService;
    }

    public function initialize($fileName)
    {
        $inputData = $this->fileParserService->parseFile($fileName);

        foreach ($inputData as $rawInstruction) {
            $lineData = explode(" ", $rawInstruction);
            array_push($this->instructionSet, array($lineData[0] => (int)$lineData[1]));
        }
    }

    /**
     * @param int $index
     * @return int
     */
    private function executeInstruction(int $index = 0): ?int
    {
        if (!in_array($index, $this->executedIndexes)) {
            array_push($this->executedIndexes, $index);
        } else {
            return $this->isPartTwo ? null : $this->accumulator;
        }

        $instructionSource = $this->isPartTwo ? $this->instructionCopy : $this->instructionSet;

        $instructionData = $instructionSource[$index];
        $instruction = array_key_first($instructionData);
        $value = $instructionData[$instruction];

        switch ($instruction) {
            case "nop":
                $index++;
                break;
            case "acc":
                $this->accumulator += $value;
                $index++;
                break;
            case "jmp":
                $index += $value;
                break;
        }

        if ($index >= count($instructionSource)) {
            return $this->accumulator;
        }

        return $this->executeInstruction($index);
    }

    /**
     * @return int
     */
    public function taskPartOne(): int
    {
        $result = $this->executeInstruction();

        if (null != $result) {
            $this->answer = $result;
        }

        return $this->answer;
    }

    /**
     * @return int
     */
    public function taskPartTwo(): int
    {
        foreach ($this->instructionSet as $commandIndex => $commandArray) {
            $commandName = array_key_first($commandArray);
            $commandValue = $commandArray[$commandName];

            if ($commandName == "acc" || $commandName == "nop") {
                continue;
            }

            $instructionObject = new ArrayObject($this->instructionSet);
            $this->instructionCopy = $instructionObject->getArrayCopy();

            $this->executedIndexes = [];
            $this->accumulator = 0;
            $this->isPartTwo = true;

            $commandName = $commandName == "jmp" ? "nop" : "jmp";
            $this->instructionCopy[$commandIndex] = array(
                $commandName => $commandValue
            );

            $result = $this->executeInstruction();

            if (null != $result) {
                $this->answer = $result;
                break;
            }
        }

        return $this->answer;
    }

    /**
     * @return array
     */
    public function getInstructionSet(): array
    {
        return $this->instructionSet;
    }
}
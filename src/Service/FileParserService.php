<?php

namespace App\Service;

use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\Exception;

/**
 * Class FileParserService
 * @package App\Service
 */
class FileParserService
{
    /**
     * @var string
     */
    private string $inputFileName = '';

    /**
     * @var string
     */
    private string $inputFileContent = '';

    /**
     * Directory path of input files to read
     */
    private const INPUT_DIRECTORY = __DIR__ . "/../Data";

    /**
     * Load file from the input directory
     */
    private function loadFile()
    {
        $fileFinder = new Finder();
        $fileFinder->files()
            ->name($this->inputFileName)
            ->in(self::INPUT_DIRECTORY);

        if ($fileFinder->hasResults()) {
            foreach ($fileFinder as $file) {
                $this->inputFileContent = $file->getContents();
            }
        } else {
            throw new FileNotFoundException();
        }


    }

    /**
     * @return \Generator
     */
    private function processLine(): \Generator
    {
        foreach (preg_split("/((\r?\n)|(\r\n?))/", $this->inputFileContent) as $line) {
            yield $line;
        }
    }

    /**
     * @param $fileName
     * @return \Generator
     */
    public function parseFile($fileName): \Generator
    {
        $this->inputFileName = $fileName;
        $this->loadFile();

        return $this->processLine();
    }
}